package com.epam.cdp.java_testing.aliaksei_tripuz.task1.model;


public class Ball extends Toy {

    private String rubber;

    public Ball(String toyName, int toyAge, double toyCost, int toySize, String rubber) {
        super(toyName, toyAge, toyCost, toySize);
        this.rubber = rubber;
    }

    public String getRubber() {
        return rubber;
    }

    public void setRubber(String rubber) {
        this.rubber = rubber;
    }

    @Override
    public void play() {
        super.play();
        setRubber("Touching the ball by its " + getRubber());

    }

    @Override
    public String toString() {
        return super.getInfo() + ", Rubber: " + getRubber();
    }
}
