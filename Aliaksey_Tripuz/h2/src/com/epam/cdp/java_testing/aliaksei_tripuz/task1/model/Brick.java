package com.epam.cdp.java_testing.aliaksei_tripuz.task1.model;


public class Brick extends Toy {

    private String edges;

    public Brick(String toyName, int toyAge, double toyCost, int toySize, String corners) {
        super(toyName, toyAge, toyCost, toySize);
        this.edges = corners;
    }

    public String getEdges() {
        return edges;
    }

    public void setEdges(String edges) {
        this.edges = edges;
    }

    @Override
    public void play() {
        super.play();
        setEdges("Kicking the brick using its " + getEdges());

    }

    @Override
    public String toString() {
        return super.getInfo() + ", Edges: " + getEdges();
    }
}
