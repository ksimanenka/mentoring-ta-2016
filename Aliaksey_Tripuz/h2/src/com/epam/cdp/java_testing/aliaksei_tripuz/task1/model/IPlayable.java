package com.epam.cdp.java_testing.aliaksei_tripuz.task1.model;


public interface IPlayable {

    void play();
}
