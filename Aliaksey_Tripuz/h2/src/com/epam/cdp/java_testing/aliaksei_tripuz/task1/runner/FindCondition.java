package com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner;


import com.epam.cdp.java_testing.aliaksei_tripuz.task1.model.Toy;

public interface FindCondition {

    boolean apply(Toy toy);

}
