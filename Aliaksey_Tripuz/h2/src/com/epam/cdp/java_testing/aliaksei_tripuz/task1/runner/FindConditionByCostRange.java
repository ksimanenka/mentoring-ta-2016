package com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner;

import com.epam.cdp.java_testing.aliaksei_tripuz.task1.model.Toy;

public class FindConditionByCostRange implements FindCondition {

    private double fromRange;
    private double toRange;


    public FindConditionByCostRange(double fromRange, double toRange) {
        this.fromRange = fromRange;
        this.toRange = toRange;
    }

    @Override
    public boolean apply(Toy toy) {
        double cost = toy.getToyCost();
        return cost >= fromRange && cost <= toRange;
    }


}