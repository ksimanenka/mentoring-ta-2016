package com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner;

import com.epam.cdp.java_testing.aliaksei_tripuz.task1.model.PlayRoom;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.model.Store;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.model.Toy;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.model.ToySizes;

import java.util.Collections;

public class Runner {

    public static void main(String[] args) {

        PlayRoom room = new PlayRoom(100, 10);
        room.addToy(Store.buyCar(ToySizes.LITTLE_SIZE));
        room.addToy(Store.buyCar(ToySizes.MIDDLE_SIZE));
        room.addToy(Store.buyCar(ToySizes.BIG_SIZE));

        room.addToy(Store.buyBall(ToySizes.LITTLE_SIZE));
        room.addToy(Store.buyBall(ToySizes.MIDDLE_SIZE));
        room.addToy(Store.buyBall(ToySizes.BIG_SIZE));

        room.addToy(Store.buyDoll(ToySizes.LITTLE_SIZE));
        room.addToy(Store.buyDoll(ToySizes.MIDDLE_SIZE));
        room.addToy(Store.buyDoll(ToySizes.BIG_SIZE));

        room.addToy(Store.buyBrick(ToySizes.LITTLE_SIZE));
        room.addToy(Store.buyBrick(ToySizes.MIDDLE_SIZE));
        room.addToy(Store.buyBrick(ToySizes.BIG_SIZE));


        Collections.sort(room.getToyList(), new ToyNameComparator());
        System.out.println("\nThis is a Play Room sorted by ToyName:");
        for (Toy toy : room.getToyList()) {
            System.out.println(toy);
        }

        Collections.sort(room.getToyList(), new ToyAgeComparator());
        System.out.println("\nThis is Play Room sorted by ToyAge:");
        for (Toy toy : room.getToyList()) {
            System.out.println(toy);
        }

        Collections.sort(room.getToyList(), new ToyCostComparator());
        System.out.println("\nThis is a Play Room sorted by ToyCost:");
        for (Toy toy : room.getToyList()) {
            System.out.println(toy);
        }

        ToyFinderByName.findToysByName("Brick", room.getToyList());
        System.out.println("\nToys found by name:");
        for (Toy toy : ToyFinderByName.getFoundToys()) {
            System.out.println(toy);
        }

        System.out.println("\nToys found by name:");
        for (Toy toy : room.find(new FindConditionByName("Brick"))) {
            System.out.println(toy);
        }

        ToyFinderByAge.findToysByAge(1,2,room.getToyList());
        System.out.println("\nToys found by age:");
        for (Toy toy : ToyFinderByAge.getFoundToys()) {
            System.out.println(toy);
        }

        System.out.println("\nToys found by age:");
        for (Toy toy : room.find(new FindConditionByAge(1, 2))) {
            System.out.println(toy);
        }

        ToyFinderBySize.findToysBySize(1, 2, room.getToyList());
        System.out.println("\nToys found by size:");
        for (Toy toy : ToyFinderBySize.getFoundToys()) {
            System.out.println(toy);
        }

        System.out.println("\nToys found by size:");
        for (Toy toy : room.find(new FindConditionByToySize(1, 2))) {
            System.out.println(toy);
        }

        ToyFinderByCost.findToysByCost(5.1, 7.2, room.getToyList());
        System.out.println("\nToys found by cost:");
        for (Toy toy : ToyFinderByCost.getFoundToys()) {
            System.out.println(toy);
        }

        System.out.println("\nToys found by cost:");
        for (Toy toy : room.find(new FindConditionByCostRange(5.1, 7.2))) {
            System.out.println(toy);
        }

        System.out.println("\nToy after playing with them:");
        for (int i = 0; i < room.getToyList().size(); i++) {
            room.getToyList().get(i).play();
            System.out.println(room.getToyList().get(i).toString());
        }

    }

}
