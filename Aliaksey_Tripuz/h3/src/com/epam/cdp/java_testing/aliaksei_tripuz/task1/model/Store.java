package com.epam.cdp.java_testing.aliaksei_tripuz.task1.model;

import java.text.DecimalFormat;
import java.util.Random;

public class Store {

    private int toyAge = new Random().nextInt(8);
    private double toyCost = Double.parseDouble(new DecimalFormat("##,##").format((2 * new Random().nextDouble() + 0.5)));

    public Car buyCar(ToySize toySize) {
        int[] wheelsNumber = {2, 4, 6};
        String wheels = wheelsNumber[new Random().nextInt(wheelsNumber.length)] + " wheels";
        return new Car("Car", toyAge, toyCost, toySize.ordinal(), wheels);
    }

    public Ball buyBall(ToySize toySize) {
        String[] rubberValues = {"thick", "thin"};
        String rubber = rubberValues[new Random().nextInt(rubberValues.length)] + " rubber";
        return new Ball("Ball", toyAge, toyCost, toySize.ordinal(), rubber);
    }

    public Doll buyDoll(ToySize toySize) {
        String[] hairValues = {"wavy", "curly", "straight"};
        String hair = hairValues[new Random().nextInt(hairValues.length)] + " hair";
        return new Doll("Doll", toyAge, toyCost, toySize.ordinal(), hair);
    }

    public Brick buyBrick(ToySize toySize) {
        String[] brickEdges = {"big", "small", "little"};
        String edge = brickEdges[new Random().nextInt(brickEdges.length)] + " edges";
        return new Brick("Brick", toyAge, toyCost, toySize.ordinal(), edge);
    }

}
