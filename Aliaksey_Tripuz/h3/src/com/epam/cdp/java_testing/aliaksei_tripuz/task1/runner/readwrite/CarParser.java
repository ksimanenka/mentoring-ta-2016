package com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.readwrite;


import com.epam.cdp.java_testing.aliaksei_tripuz.task1.exceptions.ValidateToyFieldsException;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.model.Car;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.model.Toy;

import java.util.List;

import static com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.readwrite.IStartEndToken.END_TOKEN;
import static com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.readwrite.IStartEndToken.START_TOKEN;

public class CarParser extends ToyParser {

    private String carWheels;
    private String className = Car.class.getName();

    @Override
    public Toy parseToy(List<String> toyObject) {
        for (String toyToken : toyObject) {
            if (toyObject.get(0).equals(START_TOKEN.trim() + "=" + className.trim())) {
                setToyFields(toyToken);
                if (toyToken.startsWith("carWheels")) {
                    carWheels = toyToken.substring(toyToken.lastIndexOf("=") + 1);
                }
                if (toyToken.equals(END_TOKEN.trim() + "=" + className.trim())) {
                    Car car = new Car(toyName, toyAge, toyCost, toySize, carWheels);
                    if (validateToy()) {
                        return car;
                    } else {
                        throw new ValidateToyFieldsException(
                                "Toy validation failed due to some toy field(s) illegal value.\n" +
                                        "Not added toy is: " + car);
                    }
                }
            }
        }
        return null;
    }


    @Override
    boolean validateToy() {
        return toyName != null && toyCost != 0 && carWheels != null;
    }
}
