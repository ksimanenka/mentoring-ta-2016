package com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.readwrite;


import com.epam.cdp.java_testing.aliaksei_tripuz.task1.exceptions.ReadWriteException;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.model.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.readwrite.IStartEndToken.END_TOKEN;
import static com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.readwrite.IStartEndToken.START_TOKEN;

public class ToysReader {

    public static List<Toy> read(File file) throws FileNotFoundException {
        List<List<String>> listOfToyObjs = new ArrayList<List<String>>();
        List<String> toyObj = new ArrayList<>();
        FileReader fileReader = new FileReader(file);
        try (
                BufferedReader reader = new BufferedReader(fileReader)
        ) {
            String toyField;
            while ((toyField = reader.readLine()) != null) {
                if (toyField.startsWith(START_TOKEN)) {
                    toyObj.clear();
                    toyObj.add(toyField);
                } else if (toyField.startsWith(END_TOKEN)) {
                    toyObj.add(toyField);
                    listOfToyObjs.add(toyObj);
                    toyObj = new ArrayList<>();
                } else {
                    toyObj.add(toyField);
                }
            }
        } catch (IOException e) {
            throw new ReadWriteException("Exception occurred while reading from " + file, e);
        }

        return parseToys(listOfToyObjs);
    }

    private static List<Toy> parseToys(List<List<String>> listOfToyObjs) {
        List<Toy> toysList = new ArrayList<>();

        for (List<String> toyObject : listOfToyObjs) {
            if (toyObject.get(0).startsWith(START_TOKEN)) {
                if (toyObject.get(0).contains(Car.class.getName())) {
                    toysList.add(new CarParser().parseToy(toyObject));
                }
                if (toyObject.get(0).contains(Ball.class.getName())) {
                    toysList.add(new BallParser().parseToy(toyObject));
                }
                if (toyObject.get(0).contains(Doll.class.getName())) {
                    toysList.add(new DollParser().parseToy(toyObject));
                }
                if (toyObject.get(0).contains(Brick.class.getName())) {
                    toysList.add(new BrickParser().parseToy(toyObject));
                }
            }
        }
        return toysList;
    }

}


